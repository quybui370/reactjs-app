/**
 *
 * Order
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { compose } from 'redux';
import '../../scss/style.scss';

const menu = [
  {
    id: 0,
    name: "Becky's Burgers",
    img: "1.jpg",
    desc: "Juicy burgers, crunchy fries, and creamy shakes",
    quantity: 1,
    price: 9,
  },
  {
    id: 1,
    name: "Chicken Nice",
    img: "2.jpg",
    desc: "The world's best Hainanese Chicken Rice",
    quantity: 1,
    price: 6,
  },
  {
    id: 2,
    name: "Nonna's pizza and pasta",
    img: "3.jpg",
    desc: "Classic pizza and pasta just like Nonna used to make",
    quantity: 1,
    price: 15,
  },
];

export function Order({ items, action }) {
  const chooseItems = [];
  if (action) {
    const actions = action.split(" - ");
    if (actions[0] === 'add') {
      menu[actions[1]].quantity++;
    } else if (actions[0] === 'remove' && menu[actions[1]].quantity > 0) {
      menu[actions[1]].quantity--;
    }
  }
  if (items) {
    const itemArr = items.split(",");
    itemArr.forEach((value, index) => {
      if (menu[value].quantity > 0) {
        chooseItems.push(
          <div key={index}>
            <p>{menu[value].name}: <strong>${menu[value].price*menu[value].quantity}</strong> <span>(x{menu[value].quantity})</span></p>
          </div>
        );
      }
    });
  }

  return (
    <div id="order">
      <div className="row">
        <div className="col-md-12">
          <div>{chooseItems}</div>
        </div>
      </div>
    </div>
  );
}

Order.propTypes = {
  items: PropTypes.string,
  action: PropTypes.string,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Order);
