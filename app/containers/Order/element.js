import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import LanguageProvider from 'containers/LanguageProvider';
import { ConnectedRouter } from 'connected-react-router';
import history from 'utils/history';
import Order from 'containers/Order';
import { translationMessages } from '../../i18n';
import configureStore from '../../configureStore';

// Create redux store with history
const initialState = {};
const store = configureStore(initialState, history);

export default class OrderElement extends HTMLElement {
  static get observedAttributes() {
    return ['items', 'action'];
  }

  connectedCallback() {
    this.mount();
  }

  disconnectedCallback() {
    this.unmount();
  }

  update() {
    this.unmount();
    this.mount();
  }

  attributeChangedCallback(attr, oldValue, newValue) {
    this.log(attr, oldValue, newValue);
    this.unmount();
    this.mount();
  }

  mount() {
    const props = {
      items: this.getAttribute('items'),
      action: this.getAttribute('action'),
    };
    ReactDOM.render(
      <Provider store={store}>
        <LanguageProvider messages={translationMessages}>
          <ConnectedRouter history={history}>
            <Order {...props} />
          </ConnectedRouter>
        </LanguageProvider>
      </Provider>,
      this,
    );
  }

  unmount() {
    ReactDOM.unmountComponentAtNode(this);
  }

  log(...args) {
    console.log('Attribute changed', ...args);
  }
}
