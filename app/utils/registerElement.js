import OrderElement from 'containers/Order/element';

export default function registerElement() {
  customElements.define('react-order', OrderElement);
}
